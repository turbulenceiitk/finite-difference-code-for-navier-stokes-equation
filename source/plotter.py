import numpy as np 
import matplotlib.pyplot as plt 
from mpl_toolkits.mplot3d.axes3d import Axes3D

def plotVectorField(xV,yV,title='',filename=''):
    fig = plt.figure(figsize=(10,18))
    ax1 = fig.add_subplot(211)
    ax1.quiver(xV, yV)
    ax1.set_title(title)
    if (filename):
        plt.savefig(filename)
 
#def plotScalarField(P):
#    fig = plt.figure(figsize=(10,18))
#    ax = fig.add_subplot(1, 2, 2, projection='3d')
#    ax.plot_surface(P, rstride=1, cstride=1, cmap=cm.coolwarm, linewidth=0, antialiased=False)
     
def plotFunction(functions,Start,End,width,title='',filename = '' ):
    plt.figure(figsize=(10,10))
    plt.title(title)
    t = np.arange(Start, End, width)
    legend = []
    for i in range(0,len(functions)):
        plt.plot(t, functions[i])
        legend.append(str(i))
    if (filename):
        plt.savefig(filename)
    plt.legend(legend)