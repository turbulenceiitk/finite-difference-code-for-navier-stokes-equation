import numpy as np
import macexceptions as ex
import scipy.sparse
import scipy.sparse.linalg
import scipy.linalg


class Mac:
    ##########################################################################
    ###Mesh Parameters###
    xB = 0 #Beginning of x-axis
    xE = 0 #End of x-axis
    yB = 0 #Beginning of y-axis
    yE = 0 #End of y-axis
    xN = 0 #Size of Mesh in x-direction
    yN = 0 #Size of Mesh in y-direction
    dx = 0 #Mesh Width in x-direction
    dy = 0 #Mesh Width in y-direction
    tB = 0 #Beginning time
    tE = 0 #End Time
    tN = 0 #Number of Time Steps
    dt = 0 #Time Step
    
    ###Physical Parameters###
    mu = 0    
    nu = 0 #viscosity
    rho = 1 #density
    beta = 0 #Artificial Compressiblity
    
    ###Variables Used for Computation###
    TD1xx = [] #Operator Matrices     
    TD1xy = []
    TD1yx = []
    TD1yy = []
    TD2xx = [] #Operator Matrices     
    TD2xy = []
    TD2yx = []
    TD2yy = []
    
    ###Meta Parameters###
    tindex = 0
    ##########################################################################    

    ##########################################################################
    ###Mesh Parameters Manipulations###
    def setMeshBeginX(self, xB):
        self.xB = xB
    def setMeshEndX(self, xE):
        self.xE = xE
    def setMeshBeginY(self, yB):
        self.yB = yB
    def setMeshEndY(self, yE):
        self.yE = yE
    def setMeshWidthX(self, dx):
        self.dx = dx
    def setMeshWidthY(self, dy):
        self.dy = dy
    def setTimeBegin(self, tB):
        self.tB = tB
    def setTimeEnd(self, tE):
        self.tE = tE
    def setTimeStep(self, dt):
        self.dt = dt
    def setViscosity(self, mu):
        self.mu = mu
    def setDensity(self,rho):
        self.rho = rho
    def setArtificialCompressiblityFactor(self,beta):
        self.beta = beta
        
    def printMeshParameters(self):
        print '>Mesh Parameters:'        
        print 'X-Range[xB xE]=[',self.xB,self.xE,']'
        print 'X-Mesh Width (Corrected) =', self.dx
        print 'X-Mesh Size =', self.xN
        print 'Y-Range[yB yE]=[',self.yB,self.yE,']'
        print 'Y-Mesh Width (Corrected) =', self.dy
        print 'Y-Mesh Size =', self.yN
        print 'Time Range[tB tE]=[',self.tB,self.tE,']'
        print 'Time Step (Corrected) =', self.dt
        print 'Number of Time Steps=', self.tN
        print 'Viscosity=',self.nu
    ##########################################################################
    
    ##########################################################################    
    ###System Initialization Fucntions###
    def initializeVelocity(self, xN, yN):
        xV = np.zeros([xN*yN,1])
        yV = np.zeros([xN*yN,1])
        for y in range(0, yN):
            for x in range(0,xN):
                xf = float(x)*self.dx                
                yf = float(y)*self.dy          
                xV[x + xN*y] = 4*np.sin(xf)*np.cos(yf)
        for y in range(0, yN):
            for x in range(0,xN):
                xf = float(x)*self.dx            
                yf = float(y)*self.dy                
                yV[x + xN*y] = -4*np.cos(xf)*np.sin(yf)
        return xV, yV
        
    def initializePressure(self, xN, yN):
        P = np.zeros([xN*yN,1])
        return P
        
    def initializeMesh(self):
        #Initialzing Space Paraemters        
        if(self.xE <= self.xB):
           raise ex.InconsistentMesh('X-Beginning is greater than or equal to X-End')
        if(self.yE <= self.yB):
           raise ex.InconsistentMesh('Y-Beginning is greater than or equal to Y-End')
        if(self.dx == 0):
            raise ex.InconsistentMesh('X-Mesh Width is 0')           
        if(self.dy == 0):
            raise ex.InconsistentMesh('Y-Mesh Width is 0')
        self.xN = int((self.xE-self.xB)/self.dx)
        self.dx = (self.xE-self.xB)/float(self.xN) #Corrected for Round Off Errors
        self.yN = int((self.yE-self.yB)/self.dy)
        self.dy = (self.yE-self.yB)/float(self.yN) #Corrected for Round Off Errors
        if(self.xN <= 3):
            raise ex.InconsistentMesh('Mesh Size is X-direction is less than 3')
        if(self.yN <= 3):
            raise ex.InconsistentMesh('Mesh Size is Y-direction is less than 3')
        #Initialzing Time Paraemters
        if(self.tE <= self.tB):
            raise ex.InconsistentMesh('Time-Beginning is greater than or equal to Time-End')
        if( self.dt == 0):
            raise ex.InconsistentMesh('Time Step is 0')
        self.tN = int((self.tE-self.tB)/self.dt)
        self.dt = (self.tE-self.tB)/float(self.tN) #Corrected for Round Off Errors
        #Initializing Perssure and Velocity        
        [self.xV, self.yV] = self.initializeVelocity(self.xN, self.yN)
        self.P = self.initializePressure(self.xN, self.yN)
        
        if(self.beta <= 0):
            raise ex.InvalidParameter('Artificial Compressiblity Factor should be greater than zero')
        #Initialzing Meta Parameters
        self.tindex = 0
        
        self.nu = self.mu/self.rho        
        nu = self.nu #Making typing eaiser
        dx = self.dx
        dy = self.dy
        dt = self.dt
        beta = self.beta
        rho = self.rho
        xN = self.xN
        yN = self.yN
        self.TD1xx = self.constructTriDiagOperatorX([-nu/2./dx/dx, 2./dt+nu/dx/dx, -nu/2./dx/dx ], [xN,yN])
        self.TD1xy = self.constructTriDiagOperatorX([-nu/2./dx/dx, 2./dt+nu/dx/dx, -nu/2./dx/dx ], [xN,yN])
        self.TD1yx = self.constructTriDiagOperatorY([ nu/2./dy/dy, 2./dt-nu/dy/dy,  nu/2./dy/dy ], [xN,yN])
        self.TD1yy = self.constructTriDiagOperatorX([ nu/2./dy/dy, 2./dt-nu/dy/dy,  nu/2./dy/dy ], [xN,yN])
        
        self.TD2xx = self.constructTriDiagOperatorX([ nu/2./dx/dx, 2./dt-nu/dx/dx,  nu/2./dx/dx ], [xN,yN])
        self.TD2xy = self.constructTriDiagOperatorX([ nu/2./dx/dx, 2./dt-nu/dx/dx,  nu/2./dx/dx ], [xN,yN])
        self.TD2yx = self.constructTriDiagOperatorY([-nu/2./dy/dy, 2./dt+nu/dy/dy, -nu/2./dy/dy ], [xN,yN])
        self.TD2yy = self.constructTriDiagOperatorX([-nu/2./dy/dy, 2./dt+nu/dy/dy, -nu/2./dy/dy ], [xN,yN])
        
        self.BDx = self.constructBiDiagOperatorX([-dt/2./beta/dx, dt/2./beta/dx], [xN,yN])
        self.BDy = self.constructBiDiagOperatorY([-dt/2./beta/dx, dt/2./beta/dx], [xN,yN])
        
        #Pressure Derivative Operators
        self.Dxp = self.constructBiDiagOperatorX([-1./rho/dx, 1./rho/dx],[xN,yN],1)
        self.Dyp = self.constructBiDiagOperatorY([-1./rho/dy, 1./rho/dy],[xN,yN],1)
        #Veolcity Derivative Operators
        self.Dxv = self.constructTriDiagOperatorX([-1./dx, 0, 1./dx],[xN,yN])
        self.Dyv = self.constructTriDiagOperatorY([-1./dy, 0, 1./dy],[xN,yN])
        #Operators for product of Velocities
        self.Dyuv = self.constructBiDiagOperatorY([-1./dy,1./dy],[xN,yN])
        self.Dxuv = self.constructBiDiagOperatorX([-1./dx,1./dx],[xN,yN])
        #Laplacian Operator
        self.Lap = self.constructTriDiagOperatorX([nu/2./dx/dx, -nu/dx/dx, nu/2./dx/dx],[xN,yN])+self.constructTriDiagOperatorY([nu/2./dy/dy, -nu/dy/dy, nu/2./dy/dy],[xN,yN])
        #Operator to Interpolate Velocities
        self.Ayx = self.constructBiDiagOperatorY([0.5,0.5],[xN,yN],1)
        self.Axy = self.constructBiDiagOperatorX([0.5,0.5],[xN,yN],1)
        #Operator to interpolate Velocites for calculation of Energy
        self.Axenergy = self.constructBiDiagOperatorX([0.5, 0.5],[xN,yN])
        self.Ayenergy = self.constructBiDiagOperatorY([0.5, 0.5],[xN,yN])
    ##########################################################################

    ##########################################################################    
    ###Operator Creation Functions###
    def constructTriDiagOperatorX(self, args, size, colcycle=0):
        N = size[0]
        M = size[1]        
        B = np.zeros([N, N])
        B = scipy.sparse.dok_matrix(B)        
        B[0, N-1] = args[0]
        B[0, 0] = args[1]
        B[0, 1] = args[2]
        for i in range(1,N-1):
            B[i,i-1:i+2] = args
        B[N-1, N-2] = args[0]
        B[N-1, N-1] = args[1]
        B[N-1, 0] = args[2]
        temp = [B.todense()]*M
        A = scipy.linalg.block_diag( *temp)
        return scipy.sparse.csr_matrix(A)
        
    def constructTriDiagOperatorY(self, args, size):
        N = size[0]
        M = size[1]        
        Ba = args[0]*scipy.sparse.eye(N)
        Bb = args[1]*scipy.sparse.eye(N)
        Bc = args[2]*scipy.sparse.eye(N)
        A = np.zeros([N*M, N*M])
        A = scipy.sparse.dok_matrix(A)
        A[0:N,N*(M-1):N*M] = Ba
        A[0:N,0:N] = Bb
        A[0:N,N:N*2] = Bc
        for i in range(1,M-1):
            A[i*N:(i+1)*N,N*(i-1):(N*i)] = Ba
            A[i*N:(i+1)*N,N*(i):N*(i+1)] = Bb
            A[i*N:(i+1)*N,N*(i+1):N*(i+2)] = Bc
        A[(M-1)*N:M*N, N*(M-2):N*(M-1)] = Ba
        A[(M-1)*N:M*N,N*(M-1):N*M] = Bb
        A[(M-1)*N:M*N,N*0:N] = Bc
        return scipy.sparse.csr_matrix(A)
        
    def constructBiDiagOperatorX(self, args, size, colcycle=0):
        N = size[0]
        M = size[1]        
        B = np.zeros([N, N])
        B = scipy.sparse.dok_matrix(B)
        B[0, N-1] = args[0]
        B[0, 0] = args[1]
        for i in range(1,N-1):
            B[i,i-1:i+1] = args
        B[N-1, N-2] = args[0]
        B[N-1, N-1] = args[1]
        B = B.dot(self.columnPermutationMatrix(N,colcycle))
        temp = [B.todense()]*M
        A = scipy.linalg.block_diag( *temp)
        return scipy.sparse.csr_matrix(A)
        
    def constructBiDiagOperatorY(self, args, size, colcycle=0):
        N = size[0]
        M = size[1]        
        Ba = args[0]*scipy.sparse.eye(N)
        Bb = args[1]*scipy.sparse.eye(N)
        A = np.zeros([N*M, N*M])
        A = scipy.sparse.dok_matrix(A)
        A[0:N,N*(M-1):N*M] = Ba
        A[0:N,0:N] = Bb
        for i in range(1,M-1):
            A[i*N:(i+1)*N,N*(i-1):(N*i)] = Ba
            A[i*N:(i+1)*N,N*(i):N*(i+1)] = Bb
        A[(M-1)*N:M*N, N*(M-2):N*(M-1)] = Ba
        A[(M-1)*N:M*N,N*(M-1):N*M] = Bb
        A = A.dot(self.columnPermutationMatrix(N*M, N*colcycle))
        return scipy.sparse.csr_matrix(A)
    
    def columnPermutationMatrix(self, size, d):
        N = size #Num of Columns to be permutated
        A = np.identity(N-d)
        B = np.identity(d)
        M = np.zeros([N,N])
        M[N-d:N,0:d] = B
        M[0:N-d,d:N] = A        
        return scipy.sparse.csr_matrix(M)
        
    ##########################################################################
    
    ##########################################################################
    ##Computation Functions##    
    def advanceBy(self, T): #Advance by Time Steps
        i = 0        
        while (i < T and self.tindex < self.tN):
#            print '>Shape of xV',self.xV.shape
#            print '>Shape of yV',self.yV.shape
#            print '>Shape of P',self.P.shape            
#            print '>After Calculation:'            
            #Calculation for first half            
            xyV = self.Ayx.dot(self.xV)*self.Axy.dot(self.yV) #(Vx)(Vy) product is calculated at intermediate points
#            print 'xyV.shape=',xyV.shape
            RHSx = -self.Dxp.dot(self.P)-self.xV*self.Dxv.dot(self.xV) - self.Dyuv.dot(xyV) + self.Lap.dot(self.xV)
#            print 'RHSx.shape=',RHSx.shape
            RHSy = -self.Dyp.dot(self.P)-self.yV*self.Dyv.dot(self.yV) - self.Dxuv.dot(xyV) + self.Lap.dot(self.yV)                        
#            print 'RHSy.shape=',RHSy.shape            
#            print 'Rank of TD1xx=',np.linalg.matrix_rank(self.TD1xx)            
            xV1 = scipy.sparse.linalg.lsqr(self.TD1xx, RHSx+self.TD1yx.dot(self.xV))[0]
            yV1 = scipy.sparse.linalg.lsqr(self.TD1xy, RHSy+self.TD1yy.dot(self.yV))[0]
            xV1 = np.reshape(xV1,[xV1.size,1])
            yV1 = np.reshape(yV1,[yV1.size,1])
            P1 = self.P - self.BDx.dot(xV1) - self.BDy.dot(yV1)
#            print '>Shape of xV1',xV1.shape
#            print '>Shape of yV1',yV1.shape            
#            print '>Shape of P1',P1.shape            
            #Calculation for the next Half
            xyV = self.Ayx.dot(xV1)*self.Axy.dot(yV1) #(Vx)(Vy) product is calclated at intermediate points
            RHSx = -self.Dxp.dot(P1)-xV1*self.Dxv.dot(xV1) - self.Dyuv.dot(xyV) + self.Lap.dot(xV1)
            RHSy = -self.Dyp.dot(P1)-yV1*self.Dyv.dot(yV1) - self.Dxuv.dot(xyV) + self.Lap.dot(yV1)                                    
            self.xV = scipy.sparse.linalg.lsqr(self.TD2yx,RHSx+self.TD2xx.dot(xV1))[0]
            self.yV = scipy.sparse.linalg.lsqr(self.TD2yy,RHSy+self.TD2xy.dot(yV1))[0]
            self.xV = np.reshape(self.xV,[self.xV.size,1])
            self.yV = np.reshape(self.yV,[self.yV.size,1])            
            self.P = P1 - self.BDx.dot(self.xV) - self.BDy.dot(self.yV)
#            print '>Shape of xV',self.xV.shape
#            print '>Shape of yV',self.yV.shape
#            print '>Shape of P',self.P.shape                        
            self.tindex = self.tindex + 1
            #print 'Time=',self.dt*self.tindex
            i = i+1
    ###########################################################################
    ##Probe Functions##
    def computeEnergy(self):
        xV = self.Axenergy.dot(self.xV)
        yV = self.Ayenergy.dot(self.yV)        
        xVrms_2 = np.sum(xV*xV)/(self.xN*self.yN)
        yVrms_2 = np.sum(yV*yV)/(self.xN*self.yN)
        energy = (xVrms_2 + yVrms_2)/2
        return energy
    def computeConvection(self):
        xyV = self.Ayx.dot(self.xV)*self.Axy.dot(self.yV)
        xC = self.xV*self.Dxv.dot(self.xV) + self.Dyuv.dot(xyV) #Convection in x direction
        yC  = self.yV*self.Dyv.dot(self.yV) + self.Dxuv.dot(xyV)
        C2 = xC*xC + yC*yC
        return np.sum(C2)*self.dx*self.dy
    ##########################################################################
    
    ##########################################################################    
    ##Query Functions##
    def getVelocityX(self):
        return np.reshape(self.xV,[self.xN,self.yN])
        
    def getVelocityY(self):
        return np.reshape(self.yV,[self.xN,self.yN])    
        