"""Define the 3D Mac class."""
import time
import numpy as np
import scipy.sparse
import scipy.sparse.linalg
import scipy.linalg
import yaml


import macexceptions as ex

class Mac(object):
    """The Mac class for 3D Navier Stokes equations.
    Public methods
    --------------
    1. __init__(infile): takes an input yaml configuration file and 
        sets various parameter values.
    
    2. printMeshParameters: takes no arguments and prints the parameters
        calculated by initializeMesh.
    
    3.initializeMesh: takes no parameters and calculates the mesh 
        parameters using initializeVelocity, initializePressure and
        constructTriDiagOperator1, constructTriDiagOperator2 and
        constructTriDiagOperator3.
    
    4. advanceBy(T): Calculate the fields after T timesteps and Update
        data in Mac object.

    5. getVelocityX,Y,Z: Get the velocity field after the last call to 
        advanceBy. The returned field is a three dimensional array.
    
    Private methods
    ---------------
    1. initializeVelocity: calculate initial velocity field. User must 
        edit this function to initialze a different type of velocity field.
        Nevertheless, this method should not be called from another program.
    
    2. initializePressure: counterpart of initializeVelocity for pressure
        field

    3. constructTriDiagOperator1,2,3: Construct the A matrix in the equations
        Ax=b
    4. constructRHSfirstThird, constructRHSsecondThird, constructRHSlastThird:
        Construct the b vector in Ax=b for first, second and last one third 
        of the time step.
    5. getnum: Calaculate the index of i,j,k th entry in a three dimensional
        array if it is reshaped into a vector.
    6. getijk: Calculate i,j,k for the n'th entry of a vector if it is 
        reshaped into a three dimensional array.
    7. computeEnergy: Calculate the energy for the current velocity field.
    """

    def __init__(self, infile):
        """Initialze the Mac object."""
        super(Mac, self).__init__()
        f = open(infile)
        d = yaml.load(f)
        self.xB = d['xB'] #Beginning of x-axis
        self.xE = d['xE'] #End of x-axis
        self.yB = d['yB'] #Beginning of y-axis
        self.yE = d['yE'] #End of y-axis
        self.zB = d['zB'] #Beginning of z-axis
        self.zE = d['zE'] #End of y-axis
        self.xN = d['xN'] #Size of Mesh in x-direction
        self.yN = d['yN'] #Size of Mesh in y-direction
        self.zN = d['zN'] #Size of Mesh in z-direction
        self.dx = d['dx'] #Mesh Width in x-direction
        self.dy = d['dy'] #Mesh Width in y-direction
        self.dz = d['dz'] #Mesh Width in y-direction
        self.tB = d['tB'] #Beginning time
        self.tE = d['tE'] #End Time
        self.tN = d['tN'] #Number of Time Steps
        self.dt = d['dt'] #Time Step
        self.nu = d['nu'] #viscosity
        self.rho = d['rho'] #density
        self.beta = d['beta'] #Artificial Compressiblity
        ###Meta Parameters###
        self.tindex = 0
        self.energy = []
        
    def printMeshParameters(self):
        """Print relevant mesh data."""
        print '>Mesh Parameters:'        
        print 'X-Range[xB xE]=[',self.xB,self.xE,']'
        print 'X-Mesh Width (Corrected) =', self.dx
        print 'X-Mesh Size =', self.xN
        print 'Y-Range[yB yE]=[',self.yB,self.yE,']'
        print 'Y-Mesh Width (Corrected) =', self.dy
        print 'Y-Mesh Size =', self.yN
        print 'Z-Range[zB zE]=[',self.zB,self.zE,']'
        print 'Z-Mesh Width (Corrected) =', self.dz
        print 'Z-Mesh Size =', self.zN
        print 'Time Range[tB tE]=[',self.tB,self.tE,']'
        print 'Time Step (Corrected) =', self.dt
        print 'Number of Time Steps=', self.tN

    def initializeVelocity(self, xN, yN, zN):
        """Compute the velocity field on the mesh of Mac.

        Take the mesh size in x,y and z directions. Calculate the 
        velocity field on the lxmxn mesh and return them. The returned
        fields are one dimensional vectors which can be thought of as
        reshaped 3D arrays with z as the fastest direction followed by
        y.
        """
        xV = np.zeros([xN*yN*zN,1])
        yV = np.zeros([xN*yN*zN,1])
        zV = np.zeros([xN*yN*zN,1])
        for z in range(zN):    
            for y in range(yN):
                for x in range(xN):
                    xf = float(x)*self.dx + self.xB
                    yf = float(y)*self.dy + self.yB
                    zf = float(z)*self.dz + self.zB       
                    xV[self.getnum(x,y,z)] = 4*np.cos(yf)*np.sin(xf)
                    yV[self.getnum(x,y,z)] = -4*np.cos(xf)*np.sin(yf)
                    zV[self.getnum(x,y,z)] = 0.00  
        return xV, yV, zV
        
    def initializePressure(self, xN, yN, zN):
        """Initialze the pressure field to zero."""
        P = np.zeros([xN*yN*zN,1])
        return P
        
    def initializeMesh(self):
        """Check the input provided by yaml file and calculate parameters.

        Validate parameters provided to __init__ using yaml file and raise
        exceptions if necessary.
        Calculate mesh size and adjust step size.
        Call initializeVelocity and initializePressure.
        Call constructTriDiagOperator1, constructTriDiagOperator2 and
        constructTriDiagOperator3.
        
        Exceptions: InconsistentMesh is raised if specified mesh dimesions in
            yaml file are incosistent or the mesh size calculated is too 
            small to apply ADI method.
            InvalidParameter is raised if the beta is less than 0. 
        """      
        errorMsg = '{}-Beginning is greater than or equal to {}-End'
        if(self.xE <= self.xB):
            raise ex.InconsistentMesh(errorMsg.format('X','X'))
        if(self.yE <= self.yB):
            raise ex.InconsistentMesh(errorMsg.format('Y','Y'))
        if(self.zE <= self.zB):
            raise ex.InconsistentMesh(errorMsg.format('Z','Z'))
        errorMsg = '{}-Mesh Width is 0'
        if(self.dx == 0):
            raise ex.InconsistentMesh(errorMsg.format('X'))           
        if(self.dy == 0):
            raise ex.InconsistentMesh(errorMsg.format('Y'))
        if(self.dz == 0):
            raise ex.InconsistentMesh(errorMsg.format('Z'))

        self.xN = int((self.xE-self.xB)/self.dx)
        self.dx = (self.xE-self.xB)/float(self.xN) #Correct Round-off Errors
        self.yN = int((self.yE-self.yB)/self.dy)
        self.dy = (self.yE-self.yB)/float(self.yN) #Correct Round-off Errors
        self.zN = int((self.zE-self.zB)/self.dz)
        self.dy = (self.zE-self.zB)/float(self.zN) #Correct Round-off Errors

        errorMsg = 'Mesh Size is {}-direction is less than 3'
        if(self.xN <= 3):
            raise ex.InconsistentMesh(errorMsg.format('X'))
        if(self.yN <= 3):
            raise ex.InconsistentMesh(errorMsg.format('Y'))
        if(self.zN <= 3):
            raise ex.InconsistentMesh(errorMsg.format('Z'))
        if(self.tE <= self.tB):
            errorMsg = 'Mesh Size is {}-direction is less than 3'
            raise ex.InconsistentMesh(errorMsg)
        if( self.dt == 0):
            raise ex.InconsistentMesh('Time Step is 0')

        self.tN = int((self.tE-self.tB)/self.dt)
        self.dt = (self.tE-self.tB)/float(self.tN) #Correct Round-off Errors      
        [self.xV, self.yV, self.zV] = self.initializeVelocity(self.xN, 
                                        self.yN, self.zN)
        self.P = self.initializePressure(self.xN, self.yN, self.zN)
        errorMsg = 'Artificial Compressiblity Factor must be greater than 0'
        if(self.beta <= 0):
            raise ex.InvalidParameter(errorMsg)      
        #Making typing eaiser
        nu = self.nu 
        dx = self.dx
        dy = self.dy
        dz = self.dz
        dt = self.dt
        beta = self.beta
        rho = self.rho
        xN = self.xN
        yN = self.yN
        zN = self.zN
        self.TD1 = self.constructTriDiagOperator1(xN, yN, zN, dt, nu, dz)
        self.TD2 = self.constructTriDiagOperator2(xN, yN, zN, dt, nu, dy)
        self.TD3 = self.constructTriDiagOperator3(xN, yN, zN, dt, nu, dx)

    def constructTriDiagOperator1(self, l, m, n, dt, nu, dz):
        """Create the A matrix for first one third of time step."""
        #l-->x, m-->y, n-->z
        O = np.zeros([l*m*n, l*m*n])
        for rownum in range(l*m*n):
            i,j,k = self.getijk(rownum)                
            kin = k+1 if k+1 < self.zN else 0
            kde = k-1 if k-1 >= 0 else self.zN-1
            
            colnum = self.getnum(i, j, k)
            colnumde = self.getnum(i, j, kde)
            colnumin = self.getnum(i, j, kin)

            O[rownum, colnum] = (3/dt)+2*nu/(dz**2)
            O[rownum, colnumde] = -nu/(dz**2)
            O[rownum, colnumin] = -nu/(dz**2)

        return scipy.sparse.csr_matrix(O)

    def constructTriDiagOperator2(self, l, m, n, dt, nu, dy):
        """Create the A matrix for second one third of time step."""
        #l-->x, m-->y, n-->z
        O = ((3.0/dt)+2*nu/(dy**2))*np.eye(l*m*n)

        for rownum in range(l*m*n):
            i,j,k = self.getijk(rownum)                
            jin = j+1 if j+1 < self.yN else 0
            jde = j-1 if j-1 >= 0 else self.yN-1

            colnumde = self.getnum(i, jde, k)
            colnumin = self.getnum(i, jin, k)

            O[rownum, colnumde] = -nu/(dy**2)
            O[rownum, colnumin] = -nu/(dy**2)

        return scipy.sparse.csr_matrix(O)

    def constructTriDiagOperator3(self, l, m, n, dt, nu, dx):
        """Create the A matrix for last one third of time step."""
        #l-->x, m-->y, n-->z
        O = ((3.0/dt)+2*nu/(dx**2))*np.eye(l*m*n)

        for rownum in range(l*m*n):
            i,j,k = self.getijk(rownum)                
            iin = i+1 if i+1 < self.xN else 0
            ide = i-1 if i-1 >= 0 else self.xN-1

            colnumde = self.getnum(ide, j, k)
            colnumin = self.getnum(iin, j, k)

            O[rownum, colnumde] = -nu/(dx**2)
            O[rownum, colnumin] = -nu/(dx**2)

        return scipy.sparse.csr_matrix(O)            

    def constructRHSfirstThird(self, u, v, w, p):
        """Create the b matrix for first one third of time step."""
        g = self.getnum
        RHSx = np.zeros_like(u)
        RHSy = np.zeros_like(u)
        RHSz = np.zeros_like(u)   
        for index in range(len(u)):
            i,j,k = self.getijk(index)
            kin = k+1 if k+1 < self.zN else 0
            kde = k-1 if k-1 >= 0 else self.zN-1
            jin = j+1 if j+1 < self.yN else 0
            jde = j-1 if j-1 >= 0 else self.yN-1
            iin = i+1 if i+1 < self.xN else 0
            ide = i-1 if i-1 >= 0 else self.xN-1

            RHSx[index] = -((u[g(i,j,k)]+u[g(iin,j,k)])**2 - (u[g(i,j,k)]+u[g(ide,j,k)])**2)/(4*self.dx) - \
             ((u[g(i,j,k)] + u[g(i,jin,k)])*(v[g(i,j,k)]+ v[g(iin,j,k)])-\
                 (u[g(i,jde,k)] + u[g(i,j,k)])*(v[g(i,jde,k)] + v[g(iin,jde,k)]))/(4*self.dy)-\
            ((u[g(i,j,k)]+ u[g(i,j,kin)])*(w[g(i,j,k)]+w[g(iin,j,k)])-\
                 (u[g(i,j,k)]+u[g(i,j,kde)])*(w[g(i,j,kde)]+w[g(iin,j,kde)]))/(4*self.dz) -\
             (p[g(iin,j,k)]-p[g(i,j,k)])/(self.rho * self.dx) + \
            self.nu*((u[g(iin,j,k)]-2*u[g(i,j,k)]+u[g(ide,j,k)])/(self.dx**2)+\
                  (u[g(i,jin,k)]-2*u[g(i,j,k)]+u[g(i,jde,k)])/(self.dy**2))+ 3*u[g(i,j,k)]/self.dt
                              
            RHSy[index] = -((v[g(i,j,k)]+v[g(i,jin,k)])**2 - (v[g(i,j,k)]+v[g(i,jde,k)])**2)/(4*self.dy) - \
              ((u[g(i,j,k)] + u[g(i,jin,k)])*(v[g(i,j,k)]+ v[g(iin,j,k)])-\
                  (u[g(ide,j,k)] + u[g(ide,jin,k)])*(v[g(ide,j,k)] + v[g(i,j,k)]))/(4*self.dx)-\
              ((v[g(i,j,k)]+ v[g(i,j,kin)])*(w[g(i,j,k)]+w[g(i,jin,k)])-\
                  (v[g(i,j,kde)]+v[g(i,j,k)])*(w[g(i,jin,kde)]+w[g(i,j,kde)]))/(4*self.dz) -\
              (p[g(i,jin,k)]-p[g(i,j,k)])/(self.rho * self.dx) + \
              self.nu*((v[g(iin,j,k)]-2*v[g(i,j,k)]+v[g(ide,j,k)])/(self.dx**2)+\
                  (v[g(i,jin,k)]-2*v[g(i,j,k)]+v[g(i,jde,k)])/(self.dy**2))+ 3*v[g(i,j,k)]/self.dt

            RHSz[index] = -((w[g(i,j,k)]+w[g(i,j,kin)])**2 - (w[g(i,j,k)]+w[g(i,j,kde)])**2)/(4*self.dz) - \
              ((u[g(i,j,k)] + u[g(i,j,kin)])*(w[g(i,j,k)]+ w[g(iin,j,k)])-\
                  (u[g(ide,j,k)] + u[g(ide,j,kin)])*(w[g(ide,j,k)] + w[g(i,j,k)]))/(4*self.dx)-\
              ((v[g(i,j,k)]+ v[g(i,j,kin)])*(w[g(i,j,k)]+w[g(i,jin,k)])-\
                  (v[g(i,jde,k)]+v[g(i,jde,kin)])*(w[g(i,jde,k)]+w[g(i,j,k)]))/(4*self.dy) -\
              (p[g(i,j,kin)]-p[g(i,j,k)])/(self.rho * self.dz) + \
              self.nu*((w[g(ide,j,k)]-2*w[g(i,j,k)]+w[g(iin,j,k)])/(self.dx**2)+\
                  (w[g(i,jde,k)]-2*w[g(i,j,k)]+w[g(i,jin,k)])/(self.dy**2))+ 3*w[g(i,j,k)]/self.dt

        return RHSx, RHSy, RHSz

    def constructRHSsecondThird(self, u, v, w, p):
        """Create the b matrix for second one third of time step."""
        g = self.getnum
        RHSx = np.zeros_like(u)
        RHSy = np.zeros_like(u)
        RHSz = np.zeros_like(u)   
        for index in range(len(u)):
            i,j,k = self.getijk(index)
            kin = k+1 if k+1 < self.zN else 0
            kde = k-1 if k-1 >= 0 else self.zN-1
            jin = j+1 if j+1 < self.yN else 0
            jde = j-1 if j-1 >= 0 else self.yN-1
            iin = i+1 if i+1 < self.xN else 0
            ide = i-1 if i-1 >= 0 else self.xN-1

            RHSx[index] = -((u[g(i,j,k)]+u[g(iin,j,k)])**2 - (u[g(i,j,k)]+u[g(ide,j,k)])**2)/(4*self.dx) - \
            ((u[g(i,j,k)] + u[g(i,jin,k)])*(v[g(i,j,k)]+ v[g(iin,j,k)])-\
                (u[g(i,jde,k)] + u[g(i,j,k)])*(v[g(i,jde,k)] + v[g(iin,jde,k)]))/(4*self.dy)-\
            ((u[g(i,j,k)]+ u[g(i,j,kin)])*(w[g(i,j,k)]+w[g(iin,j,k)])-\
                (u[g(i,j,k)]+u[g(i,j,kde)])*(w[g(i,j,kde)]+w[g(iin,j,kde)]))/(4*self.dz) -\
            (p[g(iin,j,k)]-p[g(i,j,k)])/(self.rho * self.dx) + \
            self.nu*((u[g(iin,j,k)]-2*u[g(i,j,k)]+u[g(ide,j,k)])/(self.dx**2)+\
                (u[g(i,j,kin)]-2*u[g(i,j,k)]+u[g(i,j,kde)])/(self.dz**2))+ 3*u[g(i,j,k)]/self.dt
                              
            RHSy[index] = -((v[g(i,j,k)]+v[g(i,jin,k)])**2 - (v[g(i,j,k)]+v[g(i,jde,k)])**2)/(4*self.dy) - \
            ((u[g(i,j,k)] + u[g(i,jin,k)])*(v[g(i,j,k)]+ v[g(iin,j,k)])-\
                (u[g(ide,j,k)] + u[g(ide,jin,k)])*(v[g(ide,j,k)] + v[g(i,j,k)]))/(4*self.dx)-\
            ((v[g(i,j,k)]+ v[g(i,j,kin)])*(w[g(i,j,k)]+w[g(i,jin,k)])-\
                (v[g(i,j,kde)]+v[g(i,j,k)])*(w[g(i,jin,kde)]+w[g(i,j,kde)]))/(4*self.dz) -\
            (p[g(i,jin,k)]-p[g(i,j,k)])/(self.rho * self.dy) + \
            self.nu*((v[g(iin,j,k)]-2*v[g(i,j,k)]+v[g(ide,j,k)])/(self.dx**2)+\
                (v[g(i,j,kde)]-2*v[g(i,j,k)]+v[g(i,j,kin)])/(self.dz**2))+ 3*v[g(i,j,k)]/self.dt

            RHSz[index] = -((w[g(i,j,k)]+w[g(i,j,kin)])**2 - (w[g(i,j,k)]+w[g(i,j,kde)])**2)/(4*self.dy) - \
            ((u[g(i,j,k)] + u[g(i,j,kin)])*(w[g(i,j,k)]+ w[g(iin,j,k)])-\
                (u[g(ide,j,k)] + u[g(ide,j,kin)])*(w[g(ide,j,k)] + w[g(i,j,k)]))/(4*self.dx)-\
            ((v[g(i,j,k)]+ v[g(i,j,kin)])*(w[g(i,j,k)]+w[g(i,jin,k)])-\
                (v[g(i,jde,k)]+v[g(i,jde,kin)])*(w[g(i,jde,k)]+w[g(i,j,k)]))/(4*self.dy) -\
            (p[g(i,j,kin)]-p[g(i,j,k)])/(self.rho * self.dz) + \
            self.nu*((w[g(ide,j,k)]-2*w[g(i,j,k)]+w[g(iin,j,k)])/(self.dx**2)+\
                (w[g(i,j,kin)]-2*w[g(i,j,k)]+w[g(i,j,kde)])/(self.dy**2))+ 3*w[g(i,j,k)]/self.dt

        return RHSx, RHSy, RHSz

    def constructRHSlastThird(self, u, v, w, p):
        """Create the b matrix for last one third of time step."""
        g = self.getnum
        RHSx = np.zeros_like(u)
        RHSy = np.zeros_like(u)
        RHSz = np.zeros_like(u)   
        for index in range(len(u)):
            i,j,k = self.getijk(index)
            kin = k+1 if k+1 < self.zN else 0
            kde = k-1 if k-1 >= 0 else self.zN-1
            jin = j+1 if j+1 < self.yN else 0
            jde = j-1 if j-1 >= 0 else self.yN-1
            iin = i+1 if i+1 < self.xN else 0
            ide = i-1 if i-1 >= 0 else self.xN-1

            RHSx[index] = -((u[g(i,j,k)]+u[g(iin,j,k)])**2 - (u[g(i,j,k)]+u[g(ide,j,k)])**2)/(4*self.dx) - \
            ((u[g(i,j,k)] + u[g(i,jin,k)])*(v[g(i,j,k)]+ v[g(iin,j,k)])-\
                (u[g(i,jde,k)] + u[g(i,j,k)])*(v[g(i,jde,k)] + v[g(iin,jde,k)]))/(4*self.dy)-\
            ((u[g(i,j,k)]+ u[g(i,j,kin)])*(w[g(i,j,k)]+w[g(iin,j,k)])-\
                (u[g(i,j,k)]+u[g(i,j,kde)])*(w[g(i,j,kde)]+w[g(iin,j,kde)]))/(4*self.dz) -\
            (p[g(iin,j,k)]-p[g(i,j,k)])/(self.rho * self.dx) + \
            self.nu*((u[g(i,j,kin)]-2*u[g(i,j,k)]+u[g(i,j,kde)])/(self.dz**2)+\
                (u[g(i,jin,k)]-2*u[g(i,j,k)]+u[g(i,jde,k)])/(self.dy**2))+ 3*u[g(i,j,k)]/self.dt
                              
            RHSy[index] = -((v[g(i,j,k)]+v[g(i,jin,k)])**2 - (v[g(i,j,k)]+v[g(i,jde,k)])**2)/(4*self.dy) - \
            ((u[g(i,j,k)] + u[g(i,jin,k)])*(v[g(i,j,k)]+ v[g(iin,j,k)])-\
                (u[g(ide,j,k)] + u[g(ide,jin,k)])*(v[g(ide,j,k)] + v[g(i,j,k)]))/(4*self.dx)-\
            ((v[g(i,j,k)]+ v[g(i,j,kin)])*(w[g(i,j,k)]+w[g(i,jin,k)])-\
                (v[g(i,j,kde)]+v[g(i,j,k)])*(w[g(i,jin,kde)]+w[g(i,j,kde)]))/(4*self.dz) -\
            (p[g(i,jin,k)]-p[g(i,j,k)])/(self.rho * self.dy) + \
            self.nu*((v[g(i,jin,k)]-2*v[g(i,j,k)]+v[g(i,jde,k)])/(self.dy**2)+\
                (v[g(i,j,kde)]-2*v[g(i,j,k)]+v[g(i,j,kin)])/(self.dz**2))+ 3*v[g(i,j,k)]/self.dt

            RHSz[index] = -((w[g(i,j,k)]+w[g(i,j,kin)])**2 - (w[g(i,j,k)]+w[g(i,j,kde)])**2)/(4*self.dy) - \
            ((u[g(i,j,k)] + u[g(i,j,kin)])*(w[g(i,j,k)]+ w[g(iin,j,k)])-\
                (u[g(ide,j,k)] + u[g(ide,j,kin)])*(w[g(ide,j,k)] + w[g(i,j,k)]))/(4*self.dx)-\
            ((v[g(i,j,k)]+ v[g(i,j,kin)])*(w[g(i,j,k)]+w[g(i,jin,k)])-\
                (v[g(i,jde,k)]+v[g(i,jde,kin)])*(w[g(i,jde,k)]+w[g(i,j,k)]))/(4*self.dy) -\
            (p[g(i,j,kin)]-p[g(i,j,k)])/(self.rho * self.dz) + \
            self.nu*((w[g(i,jde,k)]-2*w[g(i,j,k)]+w[g(i,jin,k)])/(self.dy**2)+\
                (w[g(i,j,kin)]-2*w[g(i,j,k)]+w[g(iin,j,k)])/(self.dz**2))+ 3*w[g(i,j,k)]/self.dt

        return RHSx, RHSy, RHSz

    ##Computation Functions##
    def getijk(self, num):
        """Calculate the i,j,k for nth item."""
        n = self.zN #Number of elements in row
        m = self.yN #Number of rows in stack

        num = num+1 #Start index at 1 instead of 0

        if num%(m*n) == 0:
            i = num/(m*n)
            j = m
            k = n
        else:
            i = (num/(m*n)) + 1
            num = num%(m*n)
            if num%n == 0:
                k = n
                j = num/n
            else:
                j = (num/n) + 1
                k = num%n
            
        return i-1, j-1, k-1 #Rescale to indices starting at 0

    def getnum(self, i, j, k):
        """Calculate the linearized index for ijk th item."""
        #i changes stacks
        #j changes rows
        #k changes units
        n = self.zN #Number of elements in row
        m = self.yN #Number of rows in stack

        return m*n*i + n*j + k

    def advanceBy(self, T): 
        """Advance velocity fields by t timesteps."""
        timestep = 0
        g = self.getnum
        
        while (timestep < T and self.tindex < self.tN):         
            #Calculation for first third
            #Calculate the b vector
            RHSx, RHSy, RHSz = self.constructRHSfirstThird(self.xV, self.yV, self.zV, self.P)
            
            #The A vector was already created by the initializer
            #Use it to calculate the x vector
            u1 = scipy.sparse.linalg.lsqr(self.TD1, RHSx)[0]
            v1 = scipy.sparse.linalg.lsqr(self.TD1, RHSy)[0]
            w1 = scipy.sparse.linalg.lsqr(self.TD1, RHSz)[0]
            u1= np.reshape(u1,[u1.size,1])
            v1 = np.reshape(v1,[v1.size,1])
            w1 = np.reshape(w1,[w1.size,1])
            P1 = self.P
            #Do explicit calculation for the pressure vector
            for index in range(len(P1)):
                i,j,k = self.getijk(index)
                kde = k-1 if k-1 >= 0 else self.zN-1
                jde = j-1 if j-1 >= 0 else self.yN-1
                ide = i-1 if i-1 >= 0 else self.xN-1

                P1[g(i,j,k)] = P1[g(i,j,k)] + (self.dt/(3*self.beta))*((u1[g(i,j,k)]-u1[g(ide,j,k)])/(self.dx)+\
                    (v1[g(i,j,k)]-v1[g(i,jde,k)])/(self.dy) + (w1[g(i,j,k)]-w1[g(i,j,kde)])/(self.dz))
            
            #print np.shape(u1),np.shape(v1),np.shape(w1),np.shape(P1)
            #--------------------------------------------------------------------------------------------------------
            #Calculation for the next Third
            #Calculate the b vector
            RHSx, RHSy, RHSz = self.constructRHSsecondThird(u1, v1, w1, P1)

            #The A vector was already created by the initializer
            #Use it to calculate the x vector
            u1 = scipy.sparse.linalg.lsqr(self.TD2, RHSx)[0]
            v1 = scipy.sparse.linalg.lsqr(self.TD2, RHSy)[0]
            w1 = scipy.sparse.linalg.lsqr(self.TD2, RHSz)[0]
            u1= np.reshape(u1,[u1.size,1])
            v1 = np.reshape(v1,[v1.size,1])
            w1 = np.reshape(w1,[w1.size,1])

            #Do explicit calculation for the pressure vector
            for index in range(len(P1)):
                i,j,k = self.getijk(index)
                kde = k-1 if k-1 >= 0 else self.zN-1
                jde = j-1 if j-1 >= 0 else self.yN-1
                ide = i-1 if i-1 >= 0 else self.xN-1

                P1[g(i,j,k)] = P1[g(i,j,k)] + (self.dt/(3*self.beta))*((u1[g(i,j,k)]-u1[g(ide,j,k)])/(self.dx)+\
                    (v1[g(i,j,k)]-v1[g(i,jde,k)])/(self.dy) + (w1[g(i,j,k)]-w1[g(i,j,kde)])/(self.dz))

            #print np.shape(u1),np.shape(v1),np.shape(w1),np.shape(P1)
            #------------------------------------------------------------------------------------------------------------
            #Calculation for the last Third
            #Calculate the b vector
            start = time.time()
            RHSx, RHSy, RHSz = self.constructRHSlastThird(u1, v1, w1, P1)            
            print 'Time for 3RHS Calculation:', time.time()-start
            #The A vector was already created by the initializer
            #Use it to calculate the x vector
            u1 = scipy.sparse.linalg.lsqr(self.TD3, RHSx)[0]
            v1 = scipy.sparse.linalg.lsqr(self.TD3, RHSy)[0]
            w1 = scipy.sparse.linalg.lsqr(self.TD3, RHSz)[0]
            u1= np.reshape(u1,[u1.size,1])
            v1 = np.reshape(v1,[v1.size,1])
            w1 = np.reshape(w1,[w1.size,1])

            #Do explicit calculation for the pressure vector
            for index in range(len(P1)):
                i,j,k = self.getijk(index)
                kde = k-1 if k-1 >= 0 else self.zN-1
                jde = j-1 if j-1 >= 0 else self.yN-1
                ide = i-1 if i-1 >= 0 else self.xN-1

                P1[g(i,j,k)] = P1[g(i,j,k)] + (self.dt/(3*self.beta))*((u1[g(i,j,k)]-u1[g(ide,j,k)])/(self.dx)+\
                    (v1[g(i,j,k)]-v1[g(i,jde,k)])/(self.dy) + (w1[g(i,j,k)]-w1[g(i,j,kde)])/(self.dz))
            #----------------------------------------------------------------------------------------------------------------
            #print np.shape(u1),np.shape(v1),np.shape(w1),np.shape(P1)
            #Update all values
            self.xV = u1
            self.yV = v1
            self.zV = w1  
            self.P = P1
            #Finally calculate energy                       
            self.energy.append(self.computeEnergy())
            self.tindex = self.tindex + 1
            #print 'Time=',self.dt*self.tindex            
            timestep += 1
            
    def computeEnergy(self):
        """Calculate the current energy per unit volume for the mac"""
        V2 = (self.xV*self.xV) + (self.yV*self.yV) + (self.zV*self.zV) 
        energy = np.sum(V2)/((self.xE-self.xB)*(self.yE-self.yB)*(self.zE-self.zB))
        return energy

    ##Query Functions##
    def getVelocityX(self):
        """Reshape and return the X velocity."""
        return np.reshape(self.xV,[self.xN, self.yN, self.zN])
    
    def getVelocityY(self):
        """Reshape and return the Y velocity."""
        return np.reshape(self.yV,[self.xN, self.yN, self.zN])
    
    def getVelocityZ(self):
        """Reshape and return the Z velocity."""
        return np.reshape(self.zV,[self.xN, self.yN, self.zN])   