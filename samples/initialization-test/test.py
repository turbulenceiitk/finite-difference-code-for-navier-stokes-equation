from mac import *
import numpy as np
import scipy
tub = Mac()
tub.setMeshBeginX(0.0)
tub.setMeshEndX(1)
tub.setMeshBeginY(0.0)
tub.setMeshEndY(1)
tub.setMeshWidthX(0.3)
tub.setMeshWidthY(0.3)
tub.setTimeBegin(0.0)
tub.setTimeEnd(1.5)
tub.setTimeStep(0.0007)
tub.setViscosity(0)
tub.setDensity(1)
tub.setArtificialCompressiblityFactor(0.1)
tub.initializeMesh()
tub.printMeshParameters()
A = tub.constructTriDiagonalOperatorX([1,2,3],[tub.xN-1, tub.yN])

B = tub.constructTriDiagonalOperatorY([1,2,3],[tub.xN, tub.yN-1])
C= B.todense()
