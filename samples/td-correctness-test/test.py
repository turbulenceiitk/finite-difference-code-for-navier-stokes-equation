from mac import *
import numpy as np

tub = Mac()
n = 20
M1 = np.zeros([n,n])
M2 = np.zeros([n,n])
dy = 2*np.pi/(n)
dx = dy
for y in range(0, n):
    for x in range(0,n):
        xf = float(x)*dy            
        M1[y,x] = np.sin(xf)
for y in range(0, n):
    for x in range(0,n):
        yf = float(y)*dy            
        M2[y,x] = np.sin(yf)
#Checking TriDiagonal
#D1 = np.reshape(M2,[n*n,1])
#A = tub.constructTriDiagOperatorY([-1./dx/dx,2./dx/dx, -1./dx/dx],[n,n])
#A1 = A.todense()
#D1 = A.dot(D1)
#D1 = np.reshape(M2,[n,n]) 
##initial = np.concatenate((M1[0,:].T,M1[0,:].T))
#initial = np.concatenate((M2[:,0],M2[:,0]))
##final = np.concatenate((D1[0,:].T,D1[0,:].T))
#final = np.concatenate((D1[:,0],D1[:,0]))

#Checking BiDiagonal
D1 = np.reshape(M2,[n*n,1])
A = tub.constructBiDiagOperatorY([-1./dx,1./dx],[n,n])
A1 = A.todense()
D1 = A.dot(D1)
D1 = np.reshape(D1,[n,n]) 
#initial = np.concatenate((M1[0,:].T,M1[0,:].T))
initial = np.concatenate((M2[:,0],M2[:,0]))
final = np.concatenate((D1[0,:].T,D1[0,:].T))
final = np.concatenate((D1[:,0],D1[:,0]))

