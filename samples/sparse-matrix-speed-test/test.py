##############################################################################
# A script for measuring the performance of the code by timing the speed of  
# - Matrix multiplications for TriDiagonal Matrix (X nad Y) 
# - Matrix multiplications for Bidiagonal Matrix (X and Y) 
# - Matrix Equation Solving for TriDiagonal Matrix (X nad Y)
# - Matrix Equation Solving for BiDiagonal Matrix (X nad Y)
# - TriDiagonal Matrix (X nad Y) Construction
# - BiDiagonal Matrix (X nad Y) Construction
# The time of computation is calculated for a range of Matrix Sizes.
###############################################################################

from mac import *
import numpy as np
import scipy
import time
import plotter as plt


s = 10 #Intial width fo the Mesh
e = 110 # Final width of the Mesh
iterations = 50
##Intialization of system. Matrices are created internally##
tub = Mac()

contdx = np.zeros([len(range(s,e,10)),1])
contdy = np.zeros([len(range(s,e,10)),1])
conbdx = np.zeros([len(range(s,e,10)),1])
conbdy = np.zeros([len(range(s,e,10)),1])
multdx = np.zeros([len(range(s,e,10)),1])
multdy = np.zeros([len(range(s,e,10)),1])
mulbdx = np.zeros([len(range(s,e,10)),1])
mulbdy = np.zeros([len(range(s,e,10)),1])
soltdx = np.zeros([len(range(s,e,10)),1])
soltdy = np.zeros([len(range(s,e,10)),1])
solbdx = np.zeros([len(range(s,e,10)),1])
solbdy = np.zeros([len(range(s,e,10)),1])

print 'Speed of Computation Test' 
k = 0
for i in range(s,e,10):
    print 'Calculation for size = ', i
    u = np.random.rand(i*i,1)
    scontdx = 0
    scontdy = 0
    sconbdx = 0
    sconbdy = 0
    smultdx = 0
    smultdy = 0
    smulbdx = 0 
    smulbdy = 0 
    ssoltdx = 0 
    ssoltdy = 0
    ssolbdx = 0
    ssolbdy = 0
    for n in range(0,iterations/10):
        #Construct
        start = time.time()
        tdx = tub.constructTriDiagOperatorX(np.random.rand(3),[i,i])
        end = time.time()
        scontdx = scontdx + end-start
        start = time.time()
        tdy = tub.constructTriDiagOperatorY(np.random.rand(3),[i,i])
        end = time.time()
        scontdy = scontdy + end-start
        start = time.time()
        bdx = tub.constructBiDiagOperatorX(np.random.rand(2),[i,i])
        end = time.time()
        sconbdx = sconbdx + end-start
        start = time.time()
        bdy = tub.constructBiDiagOperatorY(np.random.rand(2),[i,i])
        end = time.time()
        sconbdy = sconbdy + end-start
        for m in range(0,10):
            #Multiply
            start = time.time()
            b = tdx.dot(u)
            end = time.time()
            smultdx = smultdx + end-start
            start = time.time()
            b = tdy.dot(u)
            end = time.time()
            smultdy = smultdy + end-start
            start = time.time()
            b = bdx.dot(u)
            end = time.time()
            smulbdx = smulbdx + end-start
            start = time.time()
            b = bdy.dot(u)
            end = time.time()
            smulbdy = smulbdy + end-start
            #Solve
            start = time.time()
            b = scipy.sparse.linalg.lsqr(tdx, u)[0]
            b = np.reshape(b,[b.size,1])
            end = time.time()
            ssoltdx = ssoltdx + end-start
            start = time.time()
            b = scipy.sparse.linalg.lsqr(tdy, u)[0]
            b = np.reshape(b,[b.size,1])
            end = time.time()
            ssoltdy = ssoltdy + end-start
            start = time.time()
            b = scipy.sparse.linalg.lsqr(bdx, u)[0]
            b = np.reshape(b,[b.size,1])
            end = time.time()
            ssolbdx = ssolbdx + end-start
            start = time.time()
            b = scipy.sparse.linalg.lsqr(bdy, u)[0]
            b = np.reshape(b,[b.size,1])
            end = time.time()
            ssolbdy = ssolbdy + end-start
    contdx[k] = scontdx/n
    contdy[k] = scontdy/n
    conbdx[k] = sconbdx/n
    conbdy[k] = sconbdy/n
    multdx[k] = smultdx/n/10.
    multdy[k] = smultdy/n/10.
    mulbdx[k] = smulbdx/n/10.
    mulbdy[k] = smulbdy/n/10,
    soltdx[k] = ssoltdx/n/10.
    soltdy[k] = ssoltdy/n/10.
    solbdx[k] = ssolbdx/n/10.
    solbdy[k] = ssolbdy/n/10.
    k = k+1
#Plotting Functions
plt.plotFunction([contdx,contdy,conbdx,conbdy],s,e,10,'Time of Construction')
plt.plotFunction([multdx,multdy,mulbdx,mulbdy],s,e,10,'Time of Multiplication')
plt.plotFunction([soltdx,soltdy,solbdx,solbdy],s,e,10,'Time of Solving')
