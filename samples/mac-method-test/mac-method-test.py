from mac import *
import plotter as plt
import numpy as np
import scipy as sp
import time

tub = Mac()
tub.setMeshBeginX(0.0)
tub.setMeshEndX(2*np.pi)
tub.setMeshBeginY(0.0)
tub.setMeshEndY(2*np.pi)
tub.setMeshWidthX(0.198)
tub.setMeshWidthY(0.198)
tub.setTimeBegin(0.0)
tub.setTimeEnd(10.00)
tub.setTimeStep(0.001)
tub.setDensity(1.)
tub.setViscosity(0.1)
#Compressiblity Factor 0.001
tub.setArtificialCompressiblityFactor(0.001)
start = time.time()
tub.initializeMesh()
end = time.time()
print '>initializeMesh()|Time:', end-start
energy = np.zeros([tub.tN,1])
conv = np.zeros([tub.tN,1])
pressure = np.zeros([tub.tN,1])
tub.printMeshParameters()
plt.plotVectorField(tub.getVelocityX(),tub.getVelocityY(),'Intial Velocity Flow','initialflow1.png')

for i in range(0,tub.tN):
    
    energy[i] = tub.computeEnergy()
    conv[i] = tub.computeConvection()        
    pressure[i] = tub.P[0]
    start = time.time()        
    tub.advanceBy(1)    
    end = time.time()
    print 'Time for Full Calculation:', end - start
#print '>Time for Computation =',end-start
plt.plotVectorField(tub.getVelocityX(),tub.getVelocityY(),'Final Velocity Flow','finalflow1.png')
plt.plotFunction([energy],tub.tB,tub.tE,tub.dt,'Energy','energy1.png')
plt.plotFunction([conv],tub.tB,tub.tE,tub.dt,'Convection','convection1.png')
plt.plotFunction([pressure],tub.tB,tub.tE,tub.dt,'Pressure','pressure1.png')
